import path from 'path'

import fastify from 'fastify'
import fStatic from 'fastify-static'

const isDev = process.env.NODE_ENV === 'development'
const PORT = process.env.PORT || '8080'

async function main() {
  const app = fastify({
    logger: {
      prettyPrint: isDev
    }
  })
  
  if (isDev) {
    app.register(require('fastify-cors'))
  } else {
    app.register(fStatic, {
      root: path.resolve(__dirname, '../public')
    })
    app.setNotFoundHandler((_, reply) => {
      reply.sendFile('index.html')
    })
  }

  app.listen(parseInt(PORT), isDev ? '127.0.0.1' : '0.0.0.0', (err) => {
    if (err) throw err
  })
}

if (require.main === module) {
  main()
}
